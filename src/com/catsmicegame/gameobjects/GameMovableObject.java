package com.catsmicegame.gameobjects;

import com.catsmicegame.gamesystem.Coordinate;

public interface GameMovableObject extends GameObject{
    /**
     * Defined possible moves for objects on map to give every object the list of
     * possible moves on map
     */

    Coordinate MOVE_UP = new Coordinate(0, -1);
    Coordinate MOVE_DOWN = new Coordinate(0, 1);
    Coordinate MOVE_RIGHT = new Coordinate(1, 0);
    Coordinate MOVE_LEFT = new Coordinate(-1, 0);

    Coordinate MOVE_UP_RIGHT = MOVE_UP.addCoordinate(MOVE_RIGHT);
    Coordinate MOVE_UP_LEFT = MOVE_UP.addCoordinate(MOVE_LEFT);
    Coordinate MOVE_DOWN_RIGHT = MOVE_DOWN.addCoordinate(MOVE_RIGHT);
    Coordinate MOVE_DOWN_LEFT = MOVE_DOWN.addCoordinate(MOVE_LEFT);

    /**
     * value which will be printed on console map for certain GameObject
     */
    String toString();

    /**
     * uses the possibleMoves array to move the object to the next field
     */
    void changeLocation(Coordinate[] possibleMoves);
}
