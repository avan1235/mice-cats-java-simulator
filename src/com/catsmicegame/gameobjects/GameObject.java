package com.catsmicegame.gameobjects;

import com.catsmicegame.gamesystem.GamePosition;

/**
 * GameObject in game is something that cannot move in game
 */
public interface GameObject {

    /**
     * used to print the object on map using the given string
     * returns a String which is a characterization of the object's type on map
     */
    String getSymbol();

    /**
     * returns the position of the object on map
     */
    GamePosition getGamePosition();

    /**
     * returns the location of the image representing the object on map
     */
    String getImageLocation();

}
