package com.catsmicegame.gameobjects;

import com.catsmicegame.gamesystem.Constants;
import com.catsmicegame.gamesystem.Coordinate;
import com.catsmicegame.gamesystem.GamePosition;
import com.catsmicegame.gamesystem.Map;

import java.util.Random;

public class Cat extends Animal {

    public Cat(Map freePlacesMap){
        super(freePlacesMap);
    }

    static private int globalCatNumber = 1;

    @Override
    public Coordinate[] getPossibleMoves() {
        return new Coordinate[] {
                GameMovableObject.MOVE_DOWN,
                GameMovableObject.MOVE_UP,
                GameMovableObject.MOVE_LEFT,
                GameMovableObject.MOVE_RIGHT
        };
    }

    @Override
    public GamePosition generateInitialPosition(Map freePlaceMap) {
        Random generator = new Random();
        GamePosition candidateGamePosition;
        do {
            int x = generator.nextInt(Constants.BOARD_SIZE);
            int y = generator.nextInt(Constants.BOARD_SIZE);

            candidateGamePosition = new GamePosition(x, y);
        } while (freePlaceMap.isPlaceOccupied(candidateGamePosition));

        freePlaceMap.setPlaceOccupied(candidateGamePosition);

        return candidateGamePosition;
    }

    @Override
    public int getNumber() {
        globalCatNumber += 1;
        return globalCatNumber - 1;
    }

    @Override
    public String getName() {
        return "Kot";
    }

    @Override
    public String getSymbol() {
        return "K";
    }

    @Override
    public String getImageLocation() {
        return "cat.png";
    }
}
