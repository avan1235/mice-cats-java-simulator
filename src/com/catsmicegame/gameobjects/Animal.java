package com.catsmicegame.gameobjects;

import com.catsmicegame.gamesystem.Coordinate;
import com.catsmicegame.gamesystem.GamePosition;
import com.catsmicegame.gamesystem.Map;

import java.util.Random;

abstract public class Animal implements GameMovableObject{

    private GamePosition gamePosition;
    private int animalNumber;

    public Animal(Map freePlacesMap){
        this.gamePosition = generateInitialPosition(freePlacesMap);

        /**
         * We enumerate the all animals together with the number
         * separated for every type of animal which is incremented by getNumber function
         */
        this.animalNumber = getNumber();
    }

    /**
     * uses random number which is an index of move in possibleMoves array which will
     * be used to change gamePosition of object on map
     * tries to change the gamePosition till the success in this operation so until the
     * new gamePosition is correct
     */
    public void changeLocation(Coordinate[] possibleMoves){
        boolean changedPosition = false;
        Random generator = new Random();
        while (!changedPosition){
            int i = generator.nextInt(possibleMoves.length);
            try {

                this.gamePosition = this.gamePosition.addCoordinate(possibleMoves[i]);
                changedPosition = true;
            }
            catch (IllegalArgumentException exc){
                // do nothing, just try again to find a new gamePosition of animal
                // until it is on map
            }
        }
    }

    abstract public Coordinate[] getPossibleMoves();

    abstract public String getName();

    abstract public GamePosition generateInitialPosition(Map freePlacesMap);

    abstract public int getNumber();

    public String toString(){
        return this.getName() + " " + this.getAnimalNumber();
    }

    public int getAnimalNumber(){
        return animalNumber;
    }

    public GamePosition getGamePosition() {
        return gamePosition;
    }


}
