package com.catsmicegame.gameobjects;

import com.catsmicegame.gamesystem.Constants;
import com.catsmicegame.gamesystem.Coordinate;
import com.catsmicegame.gamesystem.GamePosition;
import com.catsmicegame.gamesystem.Map;

import java.util.Random;

public class Mouse extends Animal {

    static private int globalMouseNumber = 1;

    public Mouse(Map freePlacesMap){
        super(freePlacesMap);
    }

    @Override
    public Coordinate[] getPossibleMoves() {
        return new Coordinate[] {
                GameMovableObject.MOVE_DOWN,
                GameMovableObject.MOVE_UP,
                GameMovableObject.MOVE_LEFT,
                GameMovableObject.MOVE_RIGHT,
                GameMovableObject.MOVE_DOWN_LEFT,
                GameMovableObject.MOVE_DOWN_RIGHT,
                GameMovableObject.MOVE_UP_LEFT,
                GameMovableObject.MOVE_UP_RIGHT
        };
    }

    @Override
    public String getName() {
        return "Mysz";
    }

    /**
     * We have to remember that mice are only on the borders of map
     * at the start of the game
     */
    @Override
    public GamePosition generateInitialPosition(Map freePlacesMap) {
        Random generator = new Random();
        GamePosition candidateGamePosition;
        do {
            int p = generator.nextInt(Constants.BOARD_SIZE);
            int c = generator.nextInt(4);
            // there are four possible cases because the map is in the shape of rectangular

            if (c == 0) candidateGamePosition = new GamePosition(0, p);
            else if (c == 1) candidateGamePosition = new GamePosition(Constants.BOARD_SIZE - 1, p);
            else if (c == 2) candidateGamePosition = new GamePosition(p, 0);
            else candidateGamePosition = new GamePosition(p, Constants.BOARD_SIZE - 1);
        } while (freePlacesMap.isPlaceOccupied(candidateGamePosition));

        freePlacesMap.setPlaceOccupied(candidateGamePosition);
        return candidateGamePosition;
    }

    @Override
    public int getNumber() {
        globalMouseNumber += 1;
        return globalMouseNumber - 1;
    }

    @Override
    public String getSymbol() {
        return "M";
    }

    @Override
    public String getImageLocation() {
        return "mouse.png";
    }
}
