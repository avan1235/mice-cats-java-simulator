package com.catsmicegame.gameobjects;

import com.catsmicegame.gamesystem.Constants;
import com.catsmicegame.gamesystem.GamePosition;
import com.catsmicegame.gamesystem.Map;

public class Cheese implements GameObject {

    private GamePosition gamePosition;

    public Cheese(Map freePlaces){
        // the cheese has to be always set at map at the start of the game in the center of map
        GamePosition cheeseGamePosition = new GamePosition(Constants.BOARD_SIZE / 2, Constants.BOARD_SIZE / 2);

        freePlaces.setPlaceOccupied(cheeseGamePosition);
        this.gamePosition = cheeseGamePosition;
    }

    @Override
    public GamePosition getGamePosition(){
        return this.gamePosition;
    }

    @Override
    public String getSymbol() {
        return "S";
    }

    @Override
    public String getImageLocation() {
        return "cheese.png";
    }
}
