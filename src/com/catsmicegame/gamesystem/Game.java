package com.catsmicegame.gamesystem;

import com.catsmicegame.gameobjects.*;
import com.catsmicegame.gui.GameFrame;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Game {

    private static final String EMPTY_FILED_SYMBOL = "_";
    private static int gameRoundNumber = 1;

    private int miceLeft;
    private boolean cheeseReached;
    private boolean drawGame;
    private GameFrame gameFrame;

    /**
     * always at the beginning of the list is a cheese
     */
    private List<GameObject> gameObjects;

    public Game(){

        /**
         * paint game when it will fit on the screen
         */
        drawGame = GraphicsEnvironment.getLocalGraphicsEnvironment().
                getDefaultScreenDevice().getDisplayMode().getHeight() > Constants.FRAME_SIZE;

        drawGame = drawGame && Constants.PAINT_GRAPHICS;

        cheeseReached = false;
        miceLeft = Constants.MAX_MICE;

        Map freePlacesMap = new Map();
        gameObjects = new ArrayList<>(1 + Constants.MAX_CATS + Constants.MAX_MICE);

        // adding cheese
        gameObjects.add(new Cheese(freePlacesMap));

        // adding mice on borders
        for (int i = 0; i < Constants.MAX_MICE; i++) {
            gameObjects.add(new Mouse(freePlacesMap));
        }


        // adding cats in free places
        for (int i = 0; i < Constants.MAX_CATS; i++){
            gameObjects.add(new Cat(freePlacesMap));
        }

        System.out.println("\n=============================================\n" +
                           "POCZĄTKOWY STAN MAPY" +
                           "\n=============================================");
        printGameMap();

        if (drawGame){
            gameFrame = new GameFrame("Cats Mice Game", gameObjects);
        }
    }

    private void printGameMap(){
        System.out.println("STAN MAPY");

        String[][] printBoard = new String[Constants.BOARD_SIZE][Constants.BOARD_SIZE];

        for (int i = 0; i < Constants.BOARD_SIZE; i++){
            for (int j = 0; j < Constants.BOARD_SIZE; j++){
                printBoard[i][j] = EMPTY_FILED_SYMBOL;
            }
        }

        for (GameObject object : gameObjects){
            GamePosition p = object.getGamePosition();
            if (printBoard[p.getY()][p.getX()].equals(EMPTY_FILED_SYMBOL)){
                printBoard[p.getY()][p.getX()] = object.getSymbol();
            }
            else{
                printBoard[p.getY()][p.getX()] += "|" + object.getSymbol();
            }
        }

        for (int i = 0; i < Constants.BOARD_SIZE; i++){
            for (int j = 0; j < Constants.BOARD_SIZE; j++){
                String printString = printBoard[i][j];
                //normalize printString length to 5
                while (printString.length() < 4){
                    printString = " " + printString + " ";
                }
                System.out.printf("%5s",printString);
            }
            System.out.print("\n");
        }
    }

    private void nextRound(){
        System.out.println("\n=============================================\n" +
                           "POCZĄTEK TURY " + gameRoundNumber + " - POZOSTAŁ" +
                            (miceLeft > 1 ? (miceLeft > 4? "O" : "Y") : "A") + " " + miceLeft +
                           " MYSZ" + (miceLeft > 1 ? "Y" : "") +
                           "\n=============================================");

        for (int i = 1; i < gameObjects.size(); i++){
            GameMovableObject moveObject = (GameMovableObject) gameObjects.get(i);
            String printChange = moveObject.toString() + ": " + moveObject.getGamePosition() + " -> ";
            moveObject.changeLocation(((Animal) moveObject).getPossibleMoves());
            printChange += moveObject.getGamePosition();
            System.out.println(printChange);
        }

        checkMiceReachedCheese();
        checkCatsDinner();

        if (gameRoundNumber % Constants.DRAW_BOARD_PERIOD == 0){
            printGameMap();
        }

        gameRoundNumber += 1;
    }

    private void checkMiceReachedCheese(){
        Cheese cheese = (Cheese) gameObjects.get(0);

        List<Mouse> actMice = new ArrayList<>(Constants.MAX_MICE);

        int i = 1;
        while (i < gameObjects.size() && gameObjects.get(i).getClass() == Mouse.class){
            actMice.add((Mouse) gameObjects.get(i));
            i += 1;
        }

        for (Mouse m : actMice){
            if (m.getGamePosition().equals(cheese.getGamePosition())){
                System.out.println("UWAGA: " + m.toString() + " dotarła do sera");
                cheeseReached = true;
            }
        }
    }


    private void checkCatsDinner(){
        Cheese cheese = (Cheese) gameObjects.get(0);

        List<Mouse> actMice = new ArrayList<>(Constants.MAX_MICE);
        List<Cat> actCats = new ArrayList<>(Constants.MAX_CATS);

        int i = 1;
        while (i < gameObjects.size() && gameObjects.get(i).getClass() == Mouse.class){
            actMice.add((Mouse) gameObjects.get(i));
            i += 1;
        }

        while (i < gameObjects.size() && gameObjects.get(i).getClass() == Cat.class){
            actCats.add((Cat) gameObjects.get(i));
            i += 1;
        }

        for (int c = 0; c < actCats.size(); c++){
            for (int m = 0; m < actMice.size(); m++){
                if(actCats.get(c).getGamePosition().equals(actMice.get(m).getGamePosition())){
                    System.out.println("UWAGA: " + actCats.get(c) + " złapał " + actMice.get(m));
                    actMice.remove(m);
                    m -= 1;
                    miceLeft -= 1;
                }
            }
        }

        List<GameObject> go = new ArrayList<>();
        go.add(cheese);

        for (Mouse m : actMice){
            go.add(m);
        }
        for (Cat c : actCats){
            go.add(c);
        }

        this.gameObjects = go;
    }

    /**
     * simulates playing the game until there are no mice left or some mouse reached cheese
     */
    public void playGame(){
        while (!cheeseReached && miceLeft > 0){
            // set the delay when the game is being painted in graphics
            if (drawGame){
                try {
                    TimeUnit.MILLISECONDS.sleep(Constants.DELAY_MILLISECONDS);
                }
                catch (InterruptedException e){
                    System.out.println(e.toString());
                }
            }

            nextRound();

            // repaint the game board if the game is being painted in graphics
            if (drawGame){
                gameFrame.setGameObjects(this.gameObjects);
                gameFrame.repaint();
            }
        }

        System.out.print("\n=============================================\n" +
                "KONIEC ROZGRYWKI - " );

        if (miceLeft == 0 && cheeseReached){
            System.out.print("REMIS");
        }
        else if (miceLeft > 0 && cheeseReached){
            System.out.print("MYSZY WYGRAŁY");
        }
        else if (miceLeft == 0 && !cheeseReached) {
            System.out.print("KOTY WYGRAŁY");
        }
        else {
            System.out.print("BŁĄD ROZGRYWKI");
        }
        System.out.print("\n=============================================\n");

        printGameMap();
    }
}
