package com.catsmicegame.gamesystem;


/**
 * GamePosition class which defines the location of the object on the map
 * The location system is from 0 to BOARD_SIZE -1 in order to
 * easier understanding it in the code
 * x position increases in right direction and y increases in bottom direction
 */
public class GamePosition extends Coordinate{

    Coordinate position;

    public GamePosition(int x, int y) {
        if (x > -1 && y > -1 && x < Constants.BOARD_SIZE && y < Constants.BOARD_SIZE) {
            position = new Coordinate(x, y);
        } else {
            throw new IllegalArgumentException("Bad position given for this game in constructing object");
        }
    }

    public static GamePosition ofCoordinate(Coordinate p){
        return new GamePosition(p.getX(), p.getY());
    }

    public int getX() {
        return position.getX();
    }

    public int getY() {
        return position.getY();
    }

    public void setX(int x) {
        if (x > -1 &&x < Constants.BOARD_SIZE ) {
            position.setX(x);
        }
        else {
            throw new IllegalArgumentException("Bad position given for this game in changing object");
        }
    }

    public void setY(int y) {
        if (y > -1 && y < Constants.BOARD_SIZE) {
            position.setY(y);
        }
        else {
            throw new IllegalArgumentException("Bad position given for this game in changing object");
        }
    }

    public GamePosition addCoordinate(Coordinate p){
        return new GamePosition(this.position.getX() + p.getX(), this.position.getY() + p.getY());
    }

    @Override
    public String toString() {
        return "(" + position.getX() +", " + position.getY() +")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GamePosition that = (GamePosition) o;

        return position != null ? position.equals(that.position) : that.position == null;
    }
}
