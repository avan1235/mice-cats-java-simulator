package com.catsmicegame.gamesystem;


/**
 * which will hold information which places on map are already occupied
 * used only when creating game map because only then two objects cannot be
 * on the same place on map
 */

public class Map {
    private boolean[][] freePlace;

    public Map(){
        freePlace = new boolean[Constants.BOARD_SIZE][Constants.BOARD_SIZE];

        for (int i = 0; i < Constants.BOARD_SIZE; i++) {
            for (int j = 0; j < Constants.BOARD_SIZE; j++) {
                freePlace[i][j] = true;
            }
        }
    }

    public boolean isPlaceFree(GamePosition p){
        return freePlace[p.getX()][p.getY()];
    }

    public boolean isPlaceOccupied(GamePosition p){
        return !freePlace[p.getX()][p.getY()];
    }

    public void setPlaceFree(GamePosition p){
        this.freePlace[p.getX()][p.getY()] = true;
    }

    public void setPlaceOccupied(GamePosition p){
        this.freePlace[p.getX()][p.getY()] = false;
    }


}
