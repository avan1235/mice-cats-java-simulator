package com.catsmicegame.gamesystem;

public class Constants {
    /**
     * Size of the board of the game where
     * 2 < BOARD_SIZE < 100
     */
    public static final int BOARD_SIZE = 15;

    /**
     * Initial number of cats in game
     * 2 < MAX_CATS < 100
     */
    public static final int MAX_CATS = 20;

    /**
     * Initial number of mice in game
     * 2 < MAX_MICE < 100
     */
    public static final int MAX_MICE = 20;

    /**
     * How many rounds have to be played to
     * show the result in console
     */
    public static final int DRAW_BOARD_PERIOD = 1;

    /**
     * size of the field on game board in graphics game
     */
    public static final int FIELD_SIZE = 50;

    /**
     * offset of the map on board
     */
    public static final int FRAME_OFFSET = 15;

    /**
     * special offset of the map on board because drawing
     * from the bottom of bar
     */
    public static final int SPECIAL_OFFSET = 30;

    /**
     * graphical frame of game size based on games parameters
     */
    public static final int FRAME_SIZE = FIELD_SIZE * BOARD_SIZE + SPECIAL_OFFSET;

    /**
     * drawing object on map offset x
     */
    public static final int OBJECT_OFFSET_X = FRAME_OFFSET;

    /**
     * drawing object on map offset y
     */
    public static final int OBJECT_OFFSET_Y = FRAME_OFFSET + SPECIAL_OFFSET;

    /**
     * delay when playing the graphic game
     */
    public static final int DELAY_MILLISECONDS = 1500;

    /**
     * enable graphics in game if possible
     */
    public static final boolean PAINT_GRAPHICS = true;
}
