package com.catsmicegame.gui;

import com.catsmicegame.GameMiceCats;
import com.catsmicegame.gameobjects.GameObject;
import com.catsmicegame.gamesystem.Constants;
import com.catsmicegame.gamesystem.Game;
import com.catsmicegame.gamesystem.GamePosition;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.*;

public class GameFrame extends JFrame
{
    private List<GameObject> gameObjects;
    private String dir;

    public GameFrame(String title, List<GameObject> gameObjects)
    {
        super( title );

        this.setLayout(new FlowLayout());
        this.setSize(Constants.FRAME_SIZE, Constants.FRAME_SIZE + Constants.SPECIAL_OFFSET);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setVisible(true);

        this.gameObjects = gameObjects;
        this.dir = "";//System.getProperty("user.dir");
    }

    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        Polygon polygon = new Polygon(
                new int[]{0, 0, Constants.FRAME_SIZE, Constants.FRAME_SIZE},
                new int[]{0, Constants.FRAME_SIZE + Constants.SPECIAL_OFFSET, Constants.FRAME_SIZE + Constants.SPECIAL_OFFSET, 0},
                4);

        g.setColor(Color.white);
        g.fillPolygon(polygon);
        g.setColor(Color.black);

        // drawing the background of the game map
        for (int i = 0; i < Constants.BOARD_SIZE + 1; i++) {
            g2.draw(new Line2D.Float(
                    Constants.FRAME_OFFSET + Constants.FIELD_SIZE * i,
                    Constants.SPECIAL_OFFSET + Constants.FRAME_OFFSET,
                    Constants.FRAME_OFFSET + Constants.FIELD_SIZE * i,
                    Constants.SPECIAL_OFFSET + Constants.FRAME_OFFSET + Constants.FIELD_SIZE * Constants.BOARD_SIZE));

            g2.draw(new Line2D.Float(
                    Constants.FRAME_OFFSET,
                    Constants.SPECIAL_OFFSET + Constants.FRAME_OFFSET + Constants.FIELD_SIZE * i,
                    Constants.FRAME_OFFSET + Constants.FIELD_SIZE * Constants.BOARD_SIZE,
                    Constants.SPECIAL_OFFSET + Constants.FRAME_OFFSET + Constants.FIELD_SIZE * i));
        }

        // drawing actual objects of game which have to be actualized
        for (GameObject gameObject : gameObjects){
            GamePosition p = gameObject.getGamePosition();
            BufferedImage img;
            try {
                img =   // ImageIO.read(new File(dir + "\\images\\" + gameObject.getImageLocation()));
                        // to build to jar file has to be replaced with
                         ImageIO.read(this.getClass().getClassLoader().getResource(gameObject.getImageLocation()));
                g2.drawImage(img, null,
                        Constants.OBJECT_OFFSET_X + Constants.FIELD_SIZE * p.getX(),
                        Constants.OBJECT_OFFSET_Y + Constants.FIELD_SIZE * p.getY());
            } catch (IOException e) {
                System.out.println(e.toString());
            }

        }
    }

    public void setGameObjects(List<GameObject> gameObjects){
        this.gameObjects = gameObjects;
    }
}
