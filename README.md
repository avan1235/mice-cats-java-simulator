# Mice and cats game simulator

## About project
__*Mice cats simulator*__ game is a simple random simulator which has been
written in Java to practice inheritance and usage of project interfaces.
It emulates random behaving of cats and mice on rectangle map. Mice
want to get to the cheese but they move randomly, and cats want to eat
all mice. Every object has different set of available moves. The game
ends when all mice are eaten or any mouse get to the cheese. If cat eat
the last mouse which is already on the field with cheese then there is
draw.

## Graphics in game
There has been written gui for the game to animate the movement of animals
on map. The gui shows only when there is enough place on screen to show the
whole map which size cannot be changed.
Basic Java libraries as Swing has been used to implemnt graphics rendering in game.
<br>
<img src="https://gitlab.com/avan1235/mice-cats-java-simulator/raw/master/images/Screenshot.png" alt="Image of game" width="500"/>